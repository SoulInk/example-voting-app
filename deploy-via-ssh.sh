#!/usr/bin/env bash

# Config SSH Key
mkdir -p ~/.ssh && ssh-keyscan -H $TARGET_HOST >> ~/.ssh/known_hosts
echo -e "$TARGET_KEY" > aws_id_rsa && chmod 0600 aws_id_rsa
cat aws_id_rsa

echo "Connecting to $TARGET_USER@$TARGET_HOST"

# Connect to host and deploy
scp -i aws_id_rsa *.sh *.yml $TARGET_USER@$TARGET_HOST:/home/$TARGET_USER
ssh -i aws_id_rsa $TARGET_USER@$TARGET_HOST /bin/bash << EOF
	export CI_REGISTRY=$CI_REGISTRY
	export CI_PROJECT_PATH=$CI_PROJECT_PATH
	export CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG
	export PSQL_HOST=$PSQL_HOST
    export PSQL_PORT=$PSQL_PORT
    export PSQL_USER=$PSQL_USER
    export PSQL_PASSWORD=$PSQL_PASSWORD
    export PSQL_DB=$PSQL_DB

    mkdir worker vote result
	chmod +x *.sh
	./install-docker-aws.sh
	docker-compose down
	docker-compose pull
    docker-compose up -d --no-build --remove-orphans
EOF