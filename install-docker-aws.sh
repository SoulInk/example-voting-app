#!/usr/bin/env bash
#
# Small script used to quickly (and dirtily) install Docker on an AWS Ubuntu 18 machine
#
sudo snap install docker
sudo chmod o+rw /var/run/docker.sock # Very unsecure ! Only for testing purposes...
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
  && sudo chmod +x /usr/local/bin/docker-compose
